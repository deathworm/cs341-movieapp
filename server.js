const MYSQL = require('mysql');

let connection = MYSQL.createConnection({
        // host     : '18.220.55.156',
        host     : '127.0.0.1',
        user     : 'readuser',
        password : 'abc123',
        database : 'moviedb'
});

connection.connect(function(err) {
    if (err) {
        console.error('error connecting: ' + err.stack);
        return;
    }
    console.log('connected as id ' + connection.threadId);
});

const _ = require('lodash');
const express = require('express');
const app = express();
app.use(require('body-parser').json());

/**
 * Get Requests Blow
 */

app.get('/api/hello', (req, res) => res.send('Hello World!'));
app.get('/api/echo/:message', (req, res) => res.send(req.params.message));
app.get('/api/sql/testConnection', (req, res) => {
    connection.query('SELECT * FROM movies LIMIT 10', function (error, results, fields) {
        if (error) throw error;
        res.send(results);
    });
});
app.get('/api/sql/getRatings', (req, res) => {
    let queryString = 'SELECT * FROM ratings';

    connection.query(queryString, function(error, results, fields){
        if(error) throw error;
        res.send(results);
    });
});
app.get('/api/sql/getUserCount', (req, res) => {
    let queryString = 'SELECT count(distinct userId) as Count FROM moviedb.ratings'

    connection.query(queryString, function(error, results, fields){
        if(error) throw error;
        res.send(results);
    })
});


/**
 * Post Requests Below
 */

app.post('/api/sql/getUserMovies', (req, res) => {
    let userId = req.body.userId;
    let queryString = 'SELECT DISTINCT(imdbID) FROM ratings WHERE userID = ' + userId;

    console.log("getUSerMovies queryString", queryString);

    connection.query(queryString, function(error, results, fields){
        if(error) res.send(error);
        res.send(results);
    })
});
app.post('/api/sql/getOtherUserMovies', (req, res) => {
    let userId = req.body.userId;
    let queryString = 'SELECT DISTINCT(imdbID) FROM ratings WHERE userID != ' + userId;

    connection.query(queryString, function(error, results, fields){
        if(error) throw error;
        res.send(results);
    });
});
app.post('/api/sql/getUserRatings', (req, res) => {
    let uid = req.body.uid;
    let queryString = 'SELECT * FROM ratings WHERE userId = ' + uid;

    console.log("QueryString", queryString);

    connection.query(queryString, function(error, results, fields){
        if(error) res.send(error);
        res.send(results);
    });
});
app.post('/api/sql/getUserData', (req, res) => {
    let uid = req.body.uid;
    let queryString = 'SELECT * FROM users WHERE uid = ' + uid;

    connection.query(queryString, function(error, results, fields){
        if(error) throw error;
        res.send(results);
    });
});
app.post('/api/sql/getMovieData', (req, res) => {
    let movieId = req.body.movieId;
    let queryString = "SELECT * FROM movies WHERE movieId = " + movieId;

    connection.query(queryString, function(error, results, fields){
        if(error) return error;
        res.send(results);
    })

})

app.post('/api/sql/logIn', (req, res) => {
    let userName = req.body.userName;
    let password = req.body.password;

    let queryString = "SELECT * from users where username = '" + userName + "'";

    console.log("Login QueryString", queryString);

    connection.query(queryString, function(error, result, fields){
        if(error) res.send(error);
        console.log("login result", result);
        res.send(result);
    })
})

app.post('/api/sql/addRating', (req, res) => {
    let userID = req.body.userId;
    let imdbID = req.body.imdbID;
    let rating = req.body.rating;

    let queryString = "INSERT IGNORE INTO ratings VALUE(" + userID + ",'" + imdbID + "'," + rating + ", NOW())";

    console.log("Query String", queryString);

    connection.query(queryString, function(error, results, fields){
        if(error) res.send(error);
        res.send(results);
    })
})

app.post('/api/sql/getReviews', (req, res) => {
    let imdbID = req.body.imdbID;

    let queryString = "SELECT * FROM reviews WHERE imdbID ='" + imdbID + "' ORDER BY RAND() LIMIT 3";

    connection.query(queryString, function(error, results, fields){
        if(error) res.send(error);
        res.send(results);
    })
})

app.post('/api/sql/deleteRating', (req, res) => {
		let movieid = req.body.movieid;
		let userId = req.body.userId;

		let queryString = "DELETE FROM ratings WHERE userID = " + userId + " AND imdbID ='" + movieid + "';";

		console.log("Delete Rating queryString", queryString);

		connection.query(queryString, function(error, results, fields){
		    if(error) res.send(error);
		    console.log("Results", results);
		    res.send(results);
        });
})

app.post('/api/sql/getAverageRating', (req, res) => {
        let movieid = req.body.imdbID;

        let queryString = "SELECT ROUND(AVG(rating), 2) AS 'avg' from ratings WHERE imdbID = '"+ movieid+"';"

        console.log("Get Average Rating", queryString);

        connection.query(queryString, function(error, results, fields){
            if(error) res.send(error);
            console.log("Results", results);
            res.send(results);
        });
})

app.post('/api/sql/makeAccount', (req, res)=> {
        let username = req.body.username;
        let email = req.body.email;
        let password = req.body.password;

        let queryString = "INSERT INTO users (username, email, password) VALUES ('"+username+"','"+email+"','"+password+"');";

        console.log("Make Account queryString", queryString);

        connection.query(queryString, function(error, results, fields){
            if(error) res.send(error);
            console.log("Results", results);
            res.send(results);
        })
});

app.post('/api/sql/addReview', (req, res) => {
        let userID = req.body.userID;
        let imdbID = req.body.imdbID;
        let review = req.body.review;

        let queryString = "INSERT INTO reviews (userID, imdbID, review, date) VALUES ("+userID+", '"+imdbID+"', '"+review+"', NOW());";

        console.log("AddReview queryString", queryString);

        connection.query(queryString, function(error, results, fields){
            if(error) res.send(error);
            console.log("Add review response", results);
            res.send(results);
        })

})

app.listen(8080, () => console.log('Example app listening on port 8080!'))
