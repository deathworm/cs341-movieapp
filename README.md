This is a movie recommendation app written with ReactJS and NodeJS. The front end it written in ReactJS and the backend is written with expressJS and NodeJS.

Requirements:
- NodeJS / npm

Running the App:
1. After checking out the project, type 'npm i' 
    > - This installs all of the dependencies from the package.json
2. After the install is finished, type 'npm run start' to start the front end 
    > - This will start up the front end and open the main app in your browser
3. To run the server, type 'node server.js'
    > - To have the front end proxy the calls to the back end you will need to update the package.json
    > - External Server : "proxy": "http://18.220.55.156:8181",
    > - Local Server : "proxy" : "http://localhost:8181",
    
See server.js for connection info.