import React, {Component} from 'react';

class Bar extends Component {

    constructor(props) {
        super(props);

        this.state = {
        };
    }

    componentDidMount(){
        console.log("Bar Mounted.");
    }

    componentWillUnmount() {
    }

    render() {
        return (
            <div>
                <center>
                    <h1>Bar</h1>
                </center>
            </div>
        );
    }
}

export default Bar;