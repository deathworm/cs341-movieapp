import React, {Component} from 'react';
import '../style/App.css';
import {MenuItem, SplitButton} from 'react-bootstrap';
import * as _ from 'lodash';
import {Link} from "react-router-dom";

class Demo extends Component {

    constructor(props) {
        super(props);

        this.state = {
            results: [],
            userId: 1,
            simScores: [],
            otherUserMovies: [],
            userMovies: [],
            movieRecommendations: [],
            userData: {},
            movieData: []
        };

        this.handleChange = this.handleChange.bind(this);
        this.calculateSimScores = this.calculateSimScores.bind(this);
        this.getWatchedMovies = this.getWatchedMovies.bind(this);
        this.makeRecommendation = this.makeRecommendation.bind(this);
        this.getUserData = this.getUserData.bind(this);
        this.getMovieData = this.getMovieData.bind(this);
    }

    componentDidMount() {
    }

    componentDidUpdate() {
    }

    componentWillUnmount() {
    }

    handleChange(e) {
        this.setState({userId: e});
    }

    getRatings() {
        fetch('/api/sql/getRatings', {})
            .then((resp) => resp.json())
            .then((json) => this.setState({results: json}))
            .then(() => this.getUserData())
            .then(() => this.calculateSimScores())
            .then(() => this.getWatchedMovies())
    }

    getWatchedMovies() {
        fetch('/api/sql/getOtherUserMovies', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                userId: this.state.userId
            }),
        })
            .then((resp) => resp.json())
            .then((json) => this.setState({otherUserMovies: json}))
        fetch('/api/sql/getUserMovies', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                userId: this.state.userId
            }),
        })
            .then((resp) => resp.json())
            .then((json) => this.setState({userMovies: json}))
            .then(() => this.makeRecommendation());
    }

    getUserData() {
        fetch('/api/sql/getUserData', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                uid: this.state.userId
            }),
        })
            .then((resp) => resp.json())
            .then((json) => this.setState({userData: _.head(json)}))
    }

    getMovieData(){
        this.state.movieRecommendations.forEach((item) => {
            fetch('/api/sql/getMovieData', {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    movieId: item.movieId
                })
            })
                .then((resp) => resp.json())
                .then((json) => this.state.movieData.push(_.head(json)))
        })

        console.log("Got Movie Data", this.state.movieData);

    }

    calculateSimScores() {

        let scores = [];
        let ratingResult = this.state.results.slice();

        let results = _.remove(ratingResult, (n) => { return n.userId === this.state.userId; });
        let userData = _.remove(ratingResult, (n) => { return n.userId !== this.state.userId; });

        userData.forEach((item) => {
            let value = _.findIndex(results, (foo) => { return foo.movieId === item.movieId; });

            if (value !== -1) {
                let rating = results[value].rating;

                scores.push({
                    userId: item.userId,
                    difference: Math.pow((rating - item.rating), 2)
                });
            }
        });

        let userTrack = _.head(scores).userId;
        let total = 0;
        let simScores = [];

        scores.forEach((score) => {

            if (score.userId !== userTrack) {
                let newEntry = {userId: userTrack, simScore: (1 / (1 + Math.sqrt(total)))}

                simScores.push(newEntry);

                total = score.difference;
                userTrack = score.userId;
            }
            else {
                total += score.difference;
            }
        });

        simScores.push({userId: userTrack, simScore: (1 / (1 + Math.sqrt(total)))});

        this.setState({simScores: simScores});
    }

    makeRecommendation() {

        let recommendation = [];
        let ratingResult = this.state.results.slice();

        let unseenMovies = _.differenceBy(this.state.otherUserMovies, this.state.userMovies, 'movieId');
        let userData = _.remove(ratingResult, (n) => { return n.userId !== 1; });

        unseenMovies.forEach((movie) => {

            let numerator = 0;
            let denominator = 0;

            userData.forEach((item) => {

                if (item.movieId === movie.movieId) {

                    let simScore = _.find(this.state.simScores, function (o) { return o.userId === item.userId });

                    numerator += (simScore.simScore * item.rating);
                    denominator += simScore.simScore;
                }
            })

            recommendation.push({movieId: movie.movieId, recScore: (numerator / denominator)})
        })

        this.setState({movieRecommendations: recommendation});

        console.log("Made movie recommendations, getting movie data");

        this.getMovieData();
    }

    makeMovieLinks(movieId){
        let link = '/movie/' + movieId;

        let movieItem = _.find(this.state.movieData ? this.state.movieData : null, function(o){ return o.movieId === movieId})

        console.log("MovieItem", movieItem);

        return(<Link to={link}>{movieId}</Link>)
    }

    render() {

        let buttonString = 'Get Results For User: ' + this.state.userId;

        let uid = this.state.userData.uid ? this.state.userData.uid : '';
        let username = this.state.userData.username ? this.state.userData.username : '';
        let email = this.state.userData.email ? this.state.userData.email : '';

        return (
            <div>
                <center>
                    <SplitButton title={buttonString} pullRight id="split-button-pull-right"
                                 onClick={() => this.getRatings()} onSelect={this.handleChange}>
                        <MenuItem eventKey={1}> User 1 </MenuItem>
                        <MenuItem eventKey={2}> User 2 </MenuItem>
                        <MenuItem eventKey={3}> User 3 </MenuItem>
                        <MenuItem eventKey={4}> User 4 </MenuItem>
                        <MenuItem eventKey={5}> User 5 </MenuItem>
                        <MenuItem eventKey={6}> User 6 </MenuItem>
                    </SplitButton>
                        <h2>User Info</h2>
                        <table>
                            <tr>
                                <td>UserId: <b>{uid}</b></td>
                            </tr>
                            <tr>
                                <td>Username: <b>{username}</b></td>
                            </tr>
                            <tr>
                                <td>email: <b>{email}</b></td>
                            </tr>
                        </table>

                        <h2>User Similarity Scores</h2>
                        <table>
                            {this.state.simScores.map((item) => <tr>
                                <td>UserId: <b>{item.userId}</b></td>
                                &nbsp;
                                <td>Sim Score: <b>{item.simScore}</b></td>
                            </tr>)}
                        </table>

                        <h2>Seen Movies</h2>
                        <table>
                            {this.state.userMovies.map((item) => <tr>
                                <td>MovieId: <b>{item.movieId}</b></td>
                            </tr>)}
                        </table>

                        <h2>Movie Recommendation Scores</h2>
                        <table>
                            {this.state.movieRecommendations.map((item) => <tr>
                                <td>MovieId: <b>{item.movieId}</b></td>
                                &nbsp;
                                <td>Recommendation Score: <b>{item.recScore}</b></td>
                                &nbsp;
                                <td>Recommendation Percentage: <b>{((item.recScore / 5) * 100).toFixed(2)}%</b></td>
                            </tr>)}
                        </table>
                </center>
            </div>
        );
    }
}

export default Demo;