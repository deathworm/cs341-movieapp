import React, {Component} from 'react';
import NewMovies from './NewMovies';
import HomeRecommendations from './HomeRecommendations';
import ChucksPicks from './ChucksPicks';
import '../style/Home.css';

class Home extends Component {

    constructor(props) {
        super(props);

        this.state = {
        };
    }

    componentDidMount(){
        console.log("Home Mounted.");
    }

    componentWillUnmount() {
    }

    render() {
        return (
            <div className="Home">
                <h2>New Movies</h2>
                <NewMovies/>
                <h2>Recommended Movies</h2>
                <HomeRecommendations/>
                <h2>Chuck's Picks</h2>
                <ChucksPicks/>
            </div>
        );
    }
}

export default Home;