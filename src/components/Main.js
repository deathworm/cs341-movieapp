import React, {Component} from 'react';
import {Switch, Route} from 'react-router-dom';
import Foo from './Foo';
import Home from "./Home";
import Bar from './Bar';
import Demo from './Demo';
import Info from './Info';
import Movie from './Movie';
import User from './User';
import Search from './Search';

class Main extends Component{

    render(){
        return(
            <main>
                <Switch>
                    <Route exact path='/' component={Home}/>
                    <Route path='/demo' component={Demo}/>
                    <Route path='/user' component={User}/>
                    <Route path='/foo' component={Foo}/>
                    <Route path='/bar' component={Bar}/>
                    <Route path='/info' component={Info}/>
                    <Route path='/movie/:movieId' component={Movie}/>
                    <Route path='/search/:query' component={Search}/>
                </Switch>
            </main>
        )
    }

}

export default Main;