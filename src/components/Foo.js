import React, {Component} from 'react';

class Foo extends Component {

    constructor(props) {
        super(props);

        this.state = {
        };
    }

    componentDidMount(){
        console.log("Foo Mounted.");
    }

    componentWillUnmount() {
    }

    render() {
        return (
            <div>
                <center>
                    <h1>Foo</h1>
                </center>
            </div>
        );
    }
}

export default Foo;