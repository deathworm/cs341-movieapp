import React, {Component} from 'react';
import '../style/App.css';
import Header from './Header';
import Main from './Main';

class App extends Component {

    constructor(props) {
        super(props);

        this.state = {};

    }

    componentDidMount() {
        console.log("App Mounted.");
    }

    componentDidUpdate() {
        console.log("App Updated.");
    }

    render() {
        return (
            <div>
                <Header/>
                <p>&nbsp;&nbsp;</p>
                <p>&nbsp;</p>
                <Main/>
            </div>
        );
    }
}

export default App;
