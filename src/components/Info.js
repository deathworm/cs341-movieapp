import React, {Component} from 'react';

class Info extends Component {

    constructor(props) {
        super(props);

        this.state = {};

    }

    componentDidMount() {
        console.log("Info Mounted.");
    }

    componentDidUpdate() {
    }

    componentWillUnmount() {
    }

    render() {
        return (
            <div>
                <center>
                    <h1>Info</h1>
                </center>
                <p>This website is built using ReactJS and NodeJS. The front webpage is built using ReactJS with
                    react-routing. The react-routing is what allows for different pages to be shown without having to
                    refresh the page, or navigate off of the page. The react-routing is also how movie and search pages
                    get loaded. The search criteria get passed in on the URL and the webpage(component) uses that
                    information to draw the page. The backend is written with ExpressJS and serves as a REST endpoint
                    for all of the fetch requests. That backend service serves as an inbetween for the frontend webpage
                    and the mysql server. The MySQL server houses all of the user, rating, and review data. The data in
                    the MySQL server is used for making recommendations and the imdb-api is used to get that movie data.
                    The pages are styles with bootstrap.</p>
                <center>
                    <h3>Project By:</h3>
                    <table>
                        <b>
                            <tr>
                                <td>Brad Vasilik</td>
                            </tr>
                            <tr>
                                <td>David Harrison</td>
                            </tr>
                            <tr>
                                <td>Shannon Conlon</td>
                            </tr>
                            <tr>
                                <td>Deon Williams</td>
                            </tr>

                            <tr>
                                <td>Eric Biskin</td>
                            </tr>
                        </b>
                    </table>
                </center>
            </div>
        );
    }
}

export default Info;
