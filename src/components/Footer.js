import React, {Component} from "react";

class Footer extends Component {

    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <div className="footer"
                 style={{
                     position: 'absolute',
                     width: '100%',
                     left: 0,
                     bottom: 10,
                     right: 0
                 }}>
                <center>
                    This app is made for CS341.
                </center>
            </div>
        );
    }

}

export default Footer;