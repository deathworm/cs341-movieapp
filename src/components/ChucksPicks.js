import React, {Component} from 'react';
import {Carousel} from 'react-bootstrap';
import * as imdb from "imdb-api/lib/imdb";

class ChucksPicks extends Component {

    constructor(props) {
        super(props);

        this.state = {
            newMovies: ['tt1856101', 'tt2380307', 'tt0110912', 'tt0116483', 'tt3890160'],
            movieData: []
        }

    }

    componentDidMount() {

        this.state.newMovies.map((item) => {
            imdb.getById(item, {apiKey: 'c0b7373b', timeout: 30000})
                .then((resp) => {
                    let movieData = this.state.movieData;
                    movieData.push(resp);
                    this.setState({movieData: movieData});
                });

        })

        console.log("New Movies Mounted", this.state.movieData)
    }

    componentDidUpdate() {
        console.log("New Movies Updated", this.state.movieData);
    }

    myCarousel(items) {
        return (
            <div className="carousel-root">
                <Carousel>
                    {items.map((item, i) =>
                        <Carousel.Item key={i}>
                            <a className="thumbnail" href={`/movie/${item.imdbid}`} title={item.title}>
                                <img className="media-object"
                                     src={item.poster}
                                     alt={item.title}
                                />
                            </a>
                        </Carousel.Item>)}
                </Carousel>
            </div>
        )
    }

    render() {

        // let placeholder = "https://static1.squarespace.com/static/54e8ba93e4b07c3f655b452e/t/56c2a04520c64707756f4267/1493764650017/?format=500w";
        let movieData = this.state.movieData !== undefined ? this.state.movieData : [];

        return (
            <div>
                <center>
                    {this.myCarousel(movieData)}
                </center>
            </div>
        )

    }


}

export default ChucksPicks;