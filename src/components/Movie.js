import React, {Component} from 'react';
import * as imdb from 'imdb-api';
import {Modal, Button, ControlLabel, Form, FormGroup, Col, FormControl} from 'react-bootstrap';
import PasswordHash from "password-hash";
import * as _ from "lodash";
import HomeRecommendations from './HomeRecommendations';
import "../style/Movie.css";

class Movie extends Component {

    constructor(props) {
        super(props);

        this.state = {
            movieData: {},
            otherMovieData: {},
            makeRatingModal: false,
            rating: '4',
            userName: '',
            password: '',
            makeReviewModal: false,
            loginResponse: {},
            allowSubmit: false,
            showConfirmationModal: false,
            reviews: [],
            avgRating: [],
            review: '',
            disableSubmit: false
        };

        this.getMovieData = this.getMovieData.bind(this);
        this.showMakeRatingModal = this.showMakeRatingModal.bind(this);
        this.handlePasswordChange = this.handlePasswordChange.bind(this);
        this.handleRatingChange = this.handleRatingChange.bind(this);
        this.handleUsernameChange = this.handleUsernameChange.bind(this);
        this.showReviewModal = this.showReviewModal.bind(this);
        this.checkRatingCredintials = this.checkRatingCredintials.bind(this);
        this.showConfirmationModal = this.showConfirmationModal.bind(this);
        this.getReviews = this.getReviews.bind(this);
        this.getAverageRating = this.getAverageRating.bind(this);
        this.checkRaviewCredintials = this.checkRaviewCredintials.bind(this);
        this.handleReviewChange = this.handleReviewChange.bind(this);
    }

    componentDidMount() {
        this.getMovieData();
    }

    componentDidUpdate(props) {
        console.log("Movie Updated", this.state);
    }

    getMovieData() {

        let movieId = this.props.match.params.movieId;

        imdb.getById(movieId, {apiKey: 'c0b7373b', timeout: 30000})
            .then((foo) => this.setState({otherMovieData: foo}))
            .then(() => console.log("this.state.otherMovieData", this.state.otherMovieData))
            .then(() => this.getReviews())
            .then(() => this.getAverageRating())
            .catch(console.log);
    }

    addRating() {
        fetch('/api/sql/addRating', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                userId: this.state.loginResponse.uid,
                imdbID: this.state.otherMovieData.imdbid,
                rating: this.state.rating
            }),
        })
            .then((resp) => resp.json())
            .then((json) => {
                if (json.code) {
                    alert("Error" + json.code);
                }
                else {
                    this.setState({makeRatingModal: false, disableSubmit: false});
                    this.showConfirmationModal();
                }
            });
    }

    checkRatingCredintials() {

        this.setState({disableSubmit: true});

        console.log("Checking User Credentials");
        fetch('/api/sql/logIn', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                userName: this.state.userName,
                password: PasswordHash.generate(this.state.password)
            }),
        })
            .then((resp) => resp.json())
            .then((json) => this.setState({loginResponse: _.head(json)}))
            .then(() => console.log('LoginResponse', this.state.loginResponse))
            .then(() => this.setState({allowSubmit: PasswordHash.verify(this.state.password, this.state.loginResponse.password)}))
            .then(() => this.state.allowSubmit ? this.addRating() : console.log("Preventing Submit"));
    }

    checkRaviewCredintials() {

        this.setState({disableSubmit: true});

        console.log("Checking User Credentials");
        fetch('/api/sql/logIn', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                userName: this.state.userName,
                password: PasswordHash.generate(this.state.password)
            }),
        })
            .then((resp) => resp.json())
            .then((json) => this.setState({loginResponse: _.head(json)}))
            .then(() => console.log('LoginResponse', this.state.loginResponse))
            .then(() => this.setState({allowSubmit: PasswordHash.verify(this.state.password, this.state.loginResponse.password)}))
            .then(() => this.state.allowSubmit ? this.addReview() : console.log("Preventing Submit"));
    }

    addReview() {

        let reviewObj = {
            date: '',
            imdbID: this.props.match.params.movieId,
            review: this.state.review,
            userID: this.state.loginResponse.userid
        }

        let local = this.state.reviews;
        local.push(reviewObj);

        this.setState({reviews: local});


        fetch('/api/sql/addReview', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                userID: this.state.loginResponse.userid,
                imdbID: this.state.otherMovieData.imdbid,
                review: this.state.review
            }),
        })
            .then((resp) => resp.json())
            .then((json) => {
                if (json.code) {
                    alert("Error" + json.code);
                }
                else {
                    this.setState({makeReviewModal: false, disableSubmit: false});
                    this.showConfirmationModal();
                }
            });
    }

    getReviews() {
        fetch('/api/sql/getReviews', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                imdbID: this.props.match.params.movieId
            }),
        })
            .then((resp) => resp.json())
            .then((json) => {
                console.log("json", json)
                this.setState({reviews: json});
            })
    }

    getAverageRating() {
        fetch('/api/sql/getAverageRating', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                imdbID: this.props.match.params.movieId
            }),
        })
            .then((resp) => resp.json())
            .then((json) => {
                console.log("Response", json);
                this.setState({avgRating: _.head(json)});
            })
    }

    showMakeRatingModal() {
        this.setState({makeRatingModal: true});
    }

    showReviewModal() {
        this.setState({makeReviewModal: true})
    }

    handleRatingChange(e) {
        this.setState({rating: e.target.value});
    }

    handleUsernameChange(e) {
        this.setState({userName: e.target.value});
    }

    handlePasswordChange(e) {
        this.setState({password: e.target.value});
    }

    showConfirmationModal() {
        this.setState({showConfirmationModal: true})
    }

    handleReviewChange(e) {
        this.setState({review: e.target.value});
    }

    render() {

        let title2 = this.state.otherMovieData.title ? this.state.otherMovieData.title : '';
        let genres2 = this.state.otherMovieData.genres ? this.state.otherMovieData.genres : '';
        let year2 = this.state.otherMovieData.year ? this.state.otherMovieData.year : '';
        let poster = this.state.otherMovieData.poster ? this.state.otherMovieData.poster : '';
        let plot = this.state.otherMovieData.plot ? this.state.otherMovieData.plot : '';
        let imdburl = this.state.otherMovieData.imdburl ? this.state.otherMovieData.imdburl : '';
        let reviews = this.state.reviews ? this.state.reviews : '';
        let avg = this.state.avgRating ? this.state.avgRating.avg : '';

        console.log("Avg", avg);

        console.log("Reviews", reviews);

        console.log("allowSubmit", this.state.allowSubmit);

        // style={{border: "2px solid black", float: 'left', position: 'relative', left : '20px'}}/> </a>

        return (
            <div className="Home">

                <h1>{title2}</h1>
                <table>
                    <tr className='genre'>Genres: <b>{genres2}</b></tr>
                    <tr className='year'>Year: <b>{year2}</b></tr>
                    <tr className='plot'>Plot: {plot}</tr>
                    <tr><a href={imdburl} target="_blank"><img src={poster} alt={title2}/></a></tr>
                </table>

                <div className={avg ? `visible` : `hidden`}>
                    <h3>Average Rating</h3>
                    <div className="avg"><b>{avg} / 5.00</b></div>
                </div>

                {_.isEmpty(reviews) ? '' : <h3 className="reviews">Reviews</h3>}
                <table className='reviewTable'>
                    {reviews.map(item => {
                        // console.log("item", item);
                        return (
                            <tr>
                                <td>{item.review}</td>
                            </tr>
                        );
                    })}
                </table>

                <div className='buttonDiv'>
                    <Button onClick={this.showMakeRatingModal} bsStyle="primary">Make Rating</Button>
                    &nbsp;
                    <Button onClick={this.showReviewModal} bsStyle="success">Add Review</Button>
                </div>

                <Modal show={this.state.makeRatingModal} onHide={() => this.setState({makeRatingModal: false})}>
                    <Modal.Header closeButton>
                        <Modal.Title>Rate "{title2}"</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Form horizontal>
                            <FormGroup>
                                <Col componentClass={ControlLabel} xs={6} md={4}>Rating</Col>
                                <Col xs={4}>
                                    <FormControl value={this.state.rating} onChange={this.handleRatingChange}
                                                 componentClass="select" placeholder="Select">
                                        <option value="5">5</option>
                                        <option value="4.5">4.5</option>
                                        <option value="4">4</option>
                                        <option value="3.5">3.5</option>
                                        <option value="3">3</option>
                                        <option value="2.5">2.5</option>
                                        <option value="2">2</option>
                                        <option value="1.5">1.5</option>
                                        <option value="1">1</option>
                                        <option value=".5">1/2</option>
                                        <option value="0">0</option>
                                    </FormControl>
                                </Col>
                            </FormGroup>
                            <FormGroup>
                                <Col componentClass={ControlLabel} xs={6} md={4}>Username</Col>
                                <Col xs={4}><FormControl type='text' value={this.state.userName} placeholder='Username'
                                                         onChange={this.handleUsernameChange}/></Col>
                            </FormGroup>
                            <FormGroup>
                                <Col componentClass={ControlLabel} xs={6} md={4}>Password</Col>
                                <Col xs={4}><FormControl type='password' value={this.state.password}
                                                         placeholder='Password'
                                                         onChange={this.handlePasswordChange}/></Col>
                            </FormGroup>
                            <FormGroup>
                                <Col componentClass={ControlLabel} xs={6} md={4}/>
                                <Col xs={4}><Button bsStyle="primary"
                                                    onClick={this.checkRatingCredintials}
                                                    disabled={this.state.disableSubmit}>Submit</Button></Col>
                            </FormGroup>
                        </Form>
                    </Modal.Body>
                </Modal>

                <Modal show={this.state.showConfirmationModal}
                       onHide={() => this.setState({showConfirmationModal: false})}>
                    <Modal.Header closeButton>
                        <Modal.Title>Confirmation Modal</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        It worked.
                    </Modal.Body>
                </Modal>

                <Modal show={this.state.makeReviewModal} onHide={() => this.setState({makeReviewModal: false})}>
                    <Modal.Header closeButton>
                        <Modal.Title>Leave A Review For "{title2}"</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Form>
                            <FormGroup>
                                <Col componentClass={ControlLabel} xs={6} md={4}>Username</Col>
                                <Col xs={4}><FormControl type='text' value={this.state.userName} placeholder='Username'
                                                         onChange={this.handleUsernameChange}/></Col>
                            </FormGroup>
                            <FormGroup>
                                <Col componentClass={ControlLabel} xs={6} md={4}>Password</Col>
                                <Col xs={4}><FormControl type='password' value={this.state.password}
                                                         placeholder='Password'
                                                         onChange={this.handlePasswordChange}/></Col>
                            </FormGroup>
                            <FormGroup>
                                <Col componentClass={ControlLabel} xs={6} md={4}>Review</Col>
                                <Col xs={4}><FormControl type='text' value={this.state.review}
                                                         placeholder='Review'
                                                         onChange={this.handleReviewChange}/></Col>
                            </FormGroup>
                            <FormGroup>
                                <Col componentClass={ControlLabel} xs={6} md={4}/>
                                <Col xs={4}><Button bsStyle="primary"
                                                    onClick={this.checkRaviewCredintials}
                                                    disabled={this.state.disableSubmit}>Submit</Button></Col>
                            </FormGroup>
                        </Form>
                    </Modal.Body>
                </Modal>

                <h3>Recommended Movies</h3>
                <HomeRecommendations/>

            </div>
        );
    }
}

export default Movie;