import React, {Component} from 'react';
import "../style/Search.css";

class Search extends Component {

    constructor(props) {
        super(props);

        this.state = {
            searchData: {},
            foundSearch: false,
            responseJson: {}
        };

        this.searchImdb = this.searchImdb.bind(this);
    }

    componentDidMount() {
        let searchData = this.searchImdb();
        this.setState({searchData: searchData});

        console.log("Search Mounted.");
    }

    componentDidUpdate() {

        console.log("Search Updated.", this.state);

        if (this.state.searchData.search && !this.state.foundSearch) {
            this.forceUpdate();
        }

    }

    async searchImdb() {
        let query = this.props.match.params.query;

        let queryString = 'http://omdbapi.com/?apikey=c0b7373b&s=' + query;

        let response = await fetch(queryString);
        let responseJson = await response.json();
        let resolved = Promise.resolve(responseJson);

        console.log("responseJSON", responseJson);
        console.log("Resolved", resolved);

        this.setState({responseJson: responseJson});

        return responseJson;
    }

    render() {

        console.log("Render -- responseJson", this.state.responseJson.Search);

        let query = this.props.match.params.query;
        let searchData2 = this.state.responseJson.Search ? this.state.responseJson.Search : [];
        let error = this.state.responseJson.Error ? this.state.responseJson.Error : '';

        return (
            <div className="searchPage">
                <center>
                    <h1>First 10 Results for: "{query}"</h1>

                    <h1 style={{color: 'red'}}>{error}</h1>

                    <p>
                        <h1>Movie Table</h1>
                        <table>
                            {searchData2.map((item) => {
                                return (
                                    <tr>
                                        <td className="movieTitle">{item.Title}</td>
                                        <td><a href={`/movie/${item.imdbID}`}><img src={item.Poster} alt={item.Title}
                                                                                   style={{border: "2px solid black"}}/></a>
                                        </td>
                                    </tr>
                                )
                            })}
                        </table>
                    </p>
                </center>
            </div>
        );
    }
}

export default Search;
