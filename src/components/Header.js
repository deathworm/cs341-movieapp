import React, {Component} from 'react';
import {Button, FormControl, FormGroup, MenuItem, Nav, Navbar, NavDropdown} from "react-bootstrap";
import {Link, Redirect} from 'react-router-dom';

class Header extends Component {

    constructor(props) {
        super(props);

        this.state = {
            query: ''
        }

        this.handleQueryChange = this.handleQueryChange.bind(this);
        this.search = this.search.bind(this);
    }

    componentDidUpdate() {
        // console.log("Header Updated");
    }

    handleQueryChange(e) {
        this.setState({query: e.target.value});
    }

    search() {
        let routeString = '/search/' + this.state.query;

        console.log("Search Bar", routeString);


        let redirect = <Redirect to={routeString}/>;

        return redirect;
    }

    render() {

        let queryRoute = '/search/' + this.state.query;

        return (
            <Navbar fluid fixedTop>
                <Navbar.Header>
                    <Navbar.Brand>
                        <Link to={'/'}>C.H.U.C.K.</Link>
                    </Navbar.Brand>
                </Navbar.Header>
                <Navbar.Form pullLeft>
                    <FormGroup>
                        <FormControl type="text" placeholder="Search" value={this.state.password}
                                     onChange={this.handleQueryChange}/>
                    </FormGroup>{' '}
                    <Button type="submit" componentClass={Link} href={queryRoute} to={queryRoute}>Submit</Button>
                </Navbar.Form>
                <Nav pullRight>
                    <NavDropdown eventKey={1} title="Info" id="basic-dropdown">
                        <MenuItem eventKey={1.1} componentClass={Link} href='/info' to='/info'>Info</MenuItem>
                        {/*<MenuItem eventKey={1.3} componentClass={Link} href='/demo' to='/demo'>Demo</MenuItem>*/}
                        {/*<MenuItem divider/>*/}
                        {/*<MenuItem eventKey={1.2} href="https://bitbucket.org/deathworm/cs341-movieapp/src/master/"*/}
                                  {/*target="_blank">Source Code</MenuItem>*/}
                    </NavDropdown>
                    <NavDropdown eventKey={3} title="User" id="basic-dropdown">
                        <MenuItem eventKey={3.1} componentClass={Link} href='/user' to='/user'>User</MenuItem>
                    </NavDropdown>
                </Nav>
            </Navbar>
        )
    }

}

// export default connect(null, mapDispatchToProps)(Header);
export default Header;
