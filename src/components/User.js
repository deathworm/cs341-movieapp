import React, {Component} from 'react';
import {Button, Col, ControlLabel, Form, FormControl, FormGroup, Glyphicon, Modal} from "react-bootstrap";
import PasswordHash from "password-hash";
import * as _ from 'lodash';
import * as imdb from "imdb-api/lib/imdb";

class User extends Component {

    constructor(props) {
        super(props);

        this.state = {
            showUserInfoModal: false,
            showMakeAccountModal: false,
            userName: '',
            password: '',
            userData: {},
            loginResponse: {},
            allowLogin: false,
            movieData: [],
            userMovies: [],
            userRatings: [],
            showDeleteConfirmationModal: false,
            showDoneModal: false,
            disableButton: false,
            disableLoginButton: false,
            newUserName: '',
            newPassword: '',
            newEmail: '',
            showMadeAccountModal: false,
            disableSubmit: false
        };

        this.showUserInfoModal = this.showUserInfoModal.bind(this);
        this.showMakeAccountModal = this.showMakeAccountModal.bind(this);
        this.handleUsernameChange = this.handleUsernameChange.bind(this);
        this.handlePasswordChange = this.handlePasswordChange.bind(this);
        this.handleNewUsernameChange = this.handleNewUsernameChange.bind(this);
        this.handleNewPasswordChange = this.handleNewPasswordChange.bind(this);
        this.handleNewEmailChange = this.handleNewEmailChange.bind(this);
        this.logIn = this.logIn.bind(this);
        this.getUserMovies = this.getUserMovies.bind(this);
        this.getMovieData = this.getMovieData.bind(this);
        this.getUserRatings = this.getUserRatings.bind(this);
        this.makeAccount = this.makeAccount.bind(this);
    }

    componentDidMount() {
        console.log("User Mounted.");
    }

    componentDidUpdate() {
        // console.log("User Updated.", this.state);
    }

    showUserInfoModal() {
        this.setState({showUserInfoModal: true})
    }

    showMakeAccountModal() {
        this.setState({showMakeAccountModal: true});
    }

    handleUsernameChange(e) {
        this.setState({userName: e.target.value});
    }

    handlePasswordChange(e) {
        this.setState({password: e.target.value});
    }

    handleNewUsernameChange(e){
        this.setState({newUserName: e.target.value});
    }

    handleNewPasswordChange(e){
        this.setState({newPassword: e.target.value});
    }

    handleNewEmailChange(e){
        this.setState({newEmail: e.target.value});
    }

    logIn() {

        this.setState({disableLoginButton: true});

        console.log("Logging in");
        fetch('/api/sql/logIn', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                userName: this.state.userName,
                password: PasswordHash.generate(this.state.password)
            }),
        })
            .then((resp) => resp.json())
            .then((json) => this.setState({loginResponse: _.head(json)}))
            .then(() => this.setState({allowLogin: PasswordHash.verify(this.state.password, this.state.loginResponse.password)}))
            .then(() => this.state.allowLogin ? this.getUserMovies() : '');
    }

    makeAccount(){

        this.setState({disableSubmit: true});

        fetch('/api/sql/makeAccount', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                username: this.state.newUserName,
                email: this.state.newEmail,
                password: PasswordHash.generate(this.state.newPassword)
            }),
        })
            .then((resp) => (resp.json()))
            .then(() => this.setState({showMakeAccountModal: false, showMadeAccountModal: true}));
    }

    getUserMovies() {
        fetch('/api/sql/getUserMovies', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                userId: this.state.loginResponse.userid
            }),
        })
            .then((resp) => resp.json())
            .then((json) => this.setState({userMovies: json}))
            .then(() => console.log("Got User Movies. Now Getting Movie Data"))
            .then(() => this.getMovieData());
    }

    getUserRatings() {
        fetch('/api/sql/getUserRatings', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                uid: this.state.loginResponse.userid
            })
        })
            .then((resp) => resp.json())
            .then((json) => this.setState({userRatings: json}))
    }

    getMovieData() {

        console.log(this.state.userMovies);

        if(this.state.userMovies.code){
            console.log('The user has rated no movies, returning');
            return;
        }

        this.state.userMovies.map((item) => {
            console.log("User -- getMovieData -- userMovies item", item);
            imdb.getById(item.imdbID, {apiKey: 'c0b7373b', timeout: 30000})
                .then((resp) => {
                    let movieData = this.state.movieData;
                    movieData.push(resp);
                    this.setState({movieData: movieData});
                });
        });

        this.getUserRatings();
        this.setState({showSpinner: false});
    }

    deleteRating(item) {
        this.setState({disableButton: true});
        let movieData = this.state.movieData;
        let newData = _.pull(movieData, item);

        this.setState({movieData: newData});

        fetch('/api/sql/deleteRating', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                movieid: item.imdbid,
                userId: this.state.loginResponse.uid
            })
        })
            .then(() => this.setState({showDeleteConfirmationModal: true, disableButton: false}));
    }

    buildDeleteModal(item) {
        return (
            <div>
                <Modal show={this.state.showDeleteConfirmationModal}
                       onHide={() => this.setState({showDeleteConfirmationModal: false})}>
                    <Modal.Header closeButton>
                        <Modal.Title>Delete Rating</Modal.Title>
                        <Modal.Body>
                            The rating for <b>{item.title}</b> will be deleted.
                            <Button onClick={this.deleteRating(item)} bsStyle="danger">Delete</Button>
                        </Modal.Body>
                    </Modal.Header>
                </Modal>
            </div>
        )
    }

    render() {
        console.log("this.state.allowLogin", this.state.allowLogin);

        let movieData = this.state.movieData ? this.state.movieData : [];
        let ratingData = this.state.userRatings ? this.state.userRatings : [];

        return (
            <div>
                <Form horizontal>
                    <FormGroup>
                        <Col componentClass={ControlLabel} xs={6} md={4}>Username</Col>
                        <Col xs={4}><FormControl type='text' value={this.state.userName} placeholder='Username'
                                                 onChange={this.handleUsernameChange}/></Col>
                    </FormGroup>
                    <FormGroup>
                        <Col componentClass={ControlLabel} xs={6} md={4}>Password</Col>
                        <Col xs={4}><FormControl type='password' value={this.state.password} placeholder='Password'
                                                 onChange={this.handlePasswordChange}/></Col>
                    </FormGroup>
                    <FormGroup>
                        <Col componentClass={ControlLabel} xs={6} md={4}>Login</Col>
                        <Col xs={4}><Button bsStyle="primary" onClick={this.logIn} disabled={this.state.disableLoginButton}>Login</Button></Col>
                    </FormGroup>
                </Form>

                <center>
                    <Button onClick={this.showMakeAccountModal}>Make Account</Button>
                    {/*<Button onClick={this.showUserInfoModal}>Change User Info</Button>*/}
                </center>

                <center>
                    {this.state.allowLogin ?
                        <div>
                            <h3>Movies User Has Seen</h3>
                            <table>
                                <tr>
                                    <th>Movie Name</th>
                                    <th>Delete Rating</th>
                                </tr>
                                {movieData.map((item) => {
                                    return (
                                        <tr id='movieInfoRow'>
                                            <td id='ratingMovieName'>
                                                <a href={`/movie/${item.imdbid}`} target="_blank"
                                                   title="Will Open In New Tab">{item.title}</a>
                                            </td>
                                            <td id='ratingDeleteButton'>
                                                <Button bsSize="xsmall" value={item.imdbid} onClick={() => {
                                                    this.deleteRating(item)
                                                }} disabled={this.state.disableButton}><Glyphicon glyph="glyphicon glyphicon-remove"/></Button>
                                            </td>
                                        </tr>
                                    )
                                })}
                            </table>
                        </div>
                        : ''}
                </center>


                <Modal show={this.state.showMakeAccountModal}
                       onHide={() => this.setState({showMakeAccountModal: false})}>
                    <Modal.Header closeButton>
                        <Modal.Title>Make Account</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Form horizontal>
                            <FormGroup>
                                <Col componentClass={ControlLabel} xs={6} md={4}>Username</Col>
                                <Col xs={4}><FormControl type='text' value={this.state.newUserName} placeholder='Username'
                                                         onChange={this.handleNewUsernameChange}/></Col>
                            </FormGroup>
                            <FormGroup>
                                <Col componentClass={ControlLabel} xs={6} md={4}>Email</Col>
                                <Col xs={4}><FormControl type='email' value={this.state.newEmail} placeholder='Email'
                                                         onChange={this.handleNewEmailChange}/></Col>
                            </FormGroup>
                            <FormGroup>
                                <Col componentClass={ControlLabel} xs={6} md={4}>Password</Col>
                                <Col xs={4}><FormControl type='password' value={this.state.handleNewPasswordChange} placeholder='Password'
                                                         onChange={this.handleNewPasswordChange}/></Col>
                            </FormGroup>
                            <FormGroup>
                                <Col componentClass={ControlLabel} xs={6} md={4}>Submit</Col>
                                <Col xs={4}><Button bsStyle="primary" onClick={this.makeAccount} disabled={this.state.disableSubmit}>Submit</Button></Col>
                            </FormGroup>
                        </Form>
                    </Modal.Body>
                </Modal>

                <Modal show={this.state.showUserInfoModal} onHide={() => this.setState({showUserInfoModal: false})}>
                    <Modal.Header closeButton>
                        <Modal.Title>User Info</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        Will be added later in future version.
                    </Modal.Body>
                </Modal>

                <Modal show={this.state.showMadeAccountModal}
                       onHide={() => this.setState({showMadeAccountModal: false})}>
                    <Modal.Header closeButton>
                        <Modal.Title>Account Created</Modal.Title>
                        <Modal.Body>
                            Your account has been created. You are now able to login.
                        </Modal.Body>
                    </Modal.Header>
                </Modal>

                <Modal show={this.state.showDeleteConfirmationModal}
                       onHide={() => this.setState({showDeleteConfirmationModal: false})}>
                    <Modal.Header closeButton>
                        <Modal.Title>Rating Deleted</Modal.Title>
                        <Modal.Body>
                            The given movie has been deleted.
                        </Modal.Body>
                    </Modal.Header>
                </Modal>

                <Modal show={this.state.showDoneModal} onHide={() => this.setState({showDoneModal: false})}>
                    <Modal.Header closeButton>
                        <Modal.Title>User Info</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        Will be added later in future version.
                    </Modal.Body>
                </Modal>

            </div>
        );
    }
}

export default User;
