import React, {Component} from 'react';
import {Carousel} from 'react-bootstrap';
import * as _ from "lodash";
import * as imdb from "imdb-api/lib/imdb";

class OtherUsersRecommendations extends Component {

    constructor(props) {
        super(props);

        this.state = {
            results: [],
            userId: 4,
            simScores: [],
            otherUserMovies: [],
            userMovies: [],
            movieRecommendations: [],
            movieData: []
        };

        this.calculateSimScores = this.calculateSimScores.bind(this);
        this.getWatchedMovies = this.getWatchedMovies.bind(this);
        this.makeRecommendation = this.makeRecommendation.bind(this);
        this.getMovieData = this.getMovieData.bind(this);
        this.getMovieInformation = this.getMovieInformation.bind(this);
    }

    componentDidMount() {

        this.getRatings();

        // console.log("HomeRecommendations Mounted", this.state);
    }

    componentDidUpdate() {
        // console.log("HomeRecommendations Updated", this.state);
    }

    componentWillUnmount(){
        // console.log("HomeRecommendations Unmounted");
    }

    getRatings() {
        fetch('/api/sql/getRatings', {})
            .then((resp) => resp.json())
            .then((json) => this.setState({results: json}))
            .then(() => this.calculateSimScores())
            .then(() => this.getWatchedMovies())
    }

    getWatchedMovies() {
        fetch('/api/sql/getOtherUserMovies', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                userId: this.state.userId
            }),
        })
            .then((resp) => resp.json())
            .then((json) => this.setState({otherUserMovies: json}))
        fetch('/api/sql/getUserMovies', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                userId: this.state.userId
            }),
        })
            .then((resp) => resp.json())
            .then((json) => this.setState({userMovies: json}))
            .then(() => this.makeRecommendation());
    }

    getMovieData() {
        this.state.movieRecommendations.forEach((item) => {
            fetch('/api/sql/getMovieData', {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    movieId: item.movieId
                })
            })
                .then((resp) => resp.json())
                .then((json) => this.state.movieData.push(_.head(json)))
        })

        // console.log("Got Movie Data", this.state.movieData);
    }

    calculateSimScores() {

        let scores = [];
        let ratingResult = this.state.results.slice();

        let results = _.remove(ratingResult, (n) => {
            return n.userId === this.state.userId;
        });
        let userData = _.remove(ratingResult, (n) => {
            return n.userId !== this.state.userId;
        });

        userData.forEach((item) => {
            let value = _.findIndex(results, (foo) => {
                return foo.movieId === item.movieId;
            });

            if (value !== -1) {
                let rating = results[value].rating;

                scores.push({
                    userId: item.userId,
                    difference: Math.pow((rating - item.rating), 2)
                });
            }
        });

        let userTrack = _.head(scores).userId;
        let total = 0;
        let simScores = [];

        scores.forEach((score) => {

            if (score.userId !== userTrack) {
                let newEntry = {userId: userTrack, simScore: (1 / (1 + Math.sqrt(total)))}

                simScores.push(newEntry);

                total = score.difference;
                userTrack = score.userId;
            }
            else {
                total += score.difference;
            }
        });

        simScores.push({userId: userTrack, simScore: (1 / (1 + Math.sqrt(total)))});

        this.setState({simScores: simScores});
    }

    makeRecommendation() {

        let recommendation = [];
        let ratingResult = this.state.results.slice();

        let unseenMovies = _.differenceBy(this.state.otherUserMovies, this.state.userMovies, 'imdbID');
        let userData = _.remove(ratingResult, (n) => {
            return n.userId !== 1;
        });

        unseenMovies.forEach((movie) => {

            let numerator = 0;
            let denominator = 0;

            userData.forEach((item) => {

                if (item.imdbID === movie.imdbID) {

                    let simScore = _.find(this.state.simScores, function (o) {
                        return o.userId === item.userId
                    });

                    numerator += (simScore.simScore * item.rating);
                    denominator += simScore.simScore;
                }
            })

            recommendation.push({imdbID: movie.imdbID, recScore: (numerator / denominator)})
        })

        let sorted = _.sortBy(recommendation, [function(o){return o.recScore}]);
        let reversed = _.reverse(sorted);
        let final = _.pullAt(reversed, [0, 1, 2, 3, 4]);

        this.setState({movieRecommendations: final});

        // console.log("Made movie recommendations, getting movie data", recommendation);

        this.getMovieInformation();
    }

    getMovieInformation() {
        this.state.movieRecommendations.map((item) => {
            // console.log("item",item);
            //Doing this because it would end up undefined randomly
            if(!!item) {
                imdb.getById(item.imdbID, {apiKey: 'c0b7373b', timeout: 30000})
                    .then((resp) => {
                        let movieData = this.state.movieData;
                        movieData.push(resp);
                        this.setState({movieData: movieData});
                    });
            }
        })
    }

    myCarousel(items) {

        // console.log('MyCarousel', items);

        return (
            <div className="carousel-root">
                <Carousel>
                    {items.map((item, i) =>
                        <Carousel.Item key={i}>
                            <a className="thumbnail" href={`/movie/${item.imdbid}`} title={item.title}>
                                <img className="media-object"
                                     src={item.poster}
                                     alt={item.title}
                                />
                            </a>
                        </Carousel.Item>)}
                </Carousel>
            </div>
        )
    }

    recommendationPercentage(items) {
        return (
            <div className="table">
                <table>
                    {items.map((item, i) => {

                        // console.log("Rating Item", item);

                            return (
                                <tr>
                                    <td>IMDB ID: {item.imdbID}</td>
                                    &nbsp;
                                    <td>Recommendation Score: <a href={`/movie/${item.imdbID}`}><b>{((item.recScore / 5) * 100).toFixed(2)}%</b></a></td>
                                </tr>
                            )
                        }
                    )}
                </table>
            </div>
        )
    }

    render() {

        let movieData = this.state.movieData ? this.state.movieData : [];
        let rec = this.state.movieRecommendations ? this.state.movieRecommendations : [];

        // console.log("Movie Data", movieData);

        return (
            <div>
                <center>
                    {this.myCarousel(movieData)}
                    {/*<p>Recommendations for user: {this.state.userId}</p>*/}
                    {/*{this.recommendationPercentage(rec)}*/}
                </center>
            </div>
        )
    }

}

export default OtherUsersRecommendations;